#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import requests
import yaml
import pymongo
import datetime
import sys
import logging
import re
from bson.errors import InvalidDocument
from dateutil import parser

logger = logging.getLogger('myapp')
hdlr = logging.FileHandler(str(datetime.date.today()) + ".log")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

def dateToString(dateF):
	"""Converts dates in multiple formats to ISO date string"""
	if type(dateF) != datetime.date:
		if type(dateF) == datetime.datetime:
			dateF = dateF.date()
		else:
			try:
				dateF = parser.parse(dateF)
			except (ValueError, AttributeError):
				dateF = date.today() - datetime.timedelta(days=7)
				print("Invalid date format. Using today - 1 week as cutoff date.")
	return dateF.isoformat()

class DbConnector:
	def __init__(self, lastModified=None):
		self.loadConfig()
		self.getAuth()
		self.lastModified = lastModified
		if self.lastModified:
			self.lastModified = dateToString(lastModified)
		print("Using %s as cutoff for last modified date" % lastModified)
		logger.info("Program Started")
		
	def __enter__(self):
		self.dbConnection()
		return self
		
	def __exit__(self, *args):
		self.client.close()
		logger.info("Connection to local database closed correctly")
		
	def loadConfig(self, config_file="config.yml"):
		with open(config_file, 'r') as ymlfile:
			self.cfg = yaml.load(ymlfile)
	
	def dbConnection(self):
		self.client = pymongo.MongoClient("mongodb://%s:%s/" % (self.cfg["mongo"]["host"], self.cfg["mongo"]["port"]))
		self.dbConnection = self.client[self.cfg["mongo"]["database"]]
		logger.info("Connected to local database %s on port %s", self.cfg["mongo"]["database"], self.cfg["mongo"]["port"])
		
	def getAuth(self):
		""" POSTS necessary authentication info to the API and updates the access token"""
		body = {"grant_type": "refresh_token", "refresh_token": self.cfg["refresh_token"]}
		resp = requests.post("https://api.channeladvisor.com/oauth2/token", body, auth=(self.cfg["app_id"], self.cfg["shared_secret"]))
		self.access_token = resp.json()["access_token"]
		logger.info("Received New Authentication Token")
		
	def requestProductPage(self, url="", account_id=""):
		"""Builds the API url and return the response. 
		Filters by lastModified date if specified."""
		if url == "":
			body = {"access_token": self.access_token, "$expand": "Attributes,Images"}
			body["$filter"] = "ProfileID eq " + account_id
			#Add filter if modification date is specified.
			if self.lastModified:
				body["$filter"] = body["$filter"] + " and UpdateDateUtc ge " + self.lastModified + "Z"
			logger.info("Initial api request")
			resp = requests.get("https://api.channeladvisor.com/v1/Products", body)
		else:
			try:
				logger.info("Sent Request to %s", url)
				resp = requests.get(url)
				logger.info(resp.status_code)
			except requests.exceptions.ConnectionError as e:
				print(e)
				logger.error(e)
				logger.error("Connection Error", exc_info=True)
				logger.info("Retrying connection to %s", url)
				resp = requests.get(url)
				logger.info(resp.status_code)
		return resp
		
	def getProducts(self, account_id=""):
		"""Generator Function:
		Goes through all the response's page yielding product one by one as dictionaries"""
		url = ""
		while True:
			resp = self.requestProductPage(url, account_id=account_id)
			#Reauthenticate if access is refused
			if 400 <= resp.status_code < 500:
				logger.warning("%s, reauthenticating" % resp.status_code)
				self.getAuth()
				url = re.sub(r'(access_token=).*?(&\$)',r'\g<1>'+ self.access_token + r'\g<2>',url)
				resp = self.requestProductPage(url)
			#If request is successful get response JSON and yield products individually
			if 200 <= resp.status_code < 300:
				rj = resp.json()
				for product in rj["value"]:
					yield product
				#Go to next page of response as long as there is one
				if not "@odata.nextLink" in rj:
					logger.info("Reached last page of data")
					break
				else:
					url = rj["@odata.nextLink"]
		return
		
	def parseProductToDB(self, product):
		"""Performs any necessary data/attribute cleaning and transformation
		Then inserts the product into the database"""
		product["_id"] = product["ID"]
		product["Attributes"] = {attr["Name"]: attr["Value"] for attr in product["Attributes"]}
		#Put all printers in a list and delete individual attributes
		product["Attributes"]["Printers"] = [printer for key, printer in product["Attributes"].items() if key.startswith("CompatiblePrinter") and printer != "" and printer is not None]
		try:
			product["Images"] = {image["PlacementName"]: image["Url"] for image in product["Images"] if "." not in image["PlacementName"]}
		except InvalidDocument as e:
			logger.error(e)
			logger.error("Invalid Key", exc_info=True)
		for k in list(product["Attributes"].keys()):
			if k.startswith("CompatiblePrinter"):
				del product["Attributes"][k]
		#Remove link to next page that sometimes appears in products for unknown reasons
		product.pop("Attributes@odata.nextLink", None)
		collection = self.cfg["mongo"]["collection"] + "_" + str(product["ProfileID"])
		#This is effectively an upsert
		try:
			self.dbConnection[collection].insert_one(product)
		except pymongo.errors.DuplicateKeyError:
			self.dbConnection[collection].update({"_id": product["_id"]}, product)
			
	def insertAllProducts(self):
		i = 0
		for account_id in self.cfg["account_ids"]:
			for product in self.getProducts(account_id):
				self.parseProductToDB(product)
				i+=1
				if i%1000 == 0:
					print("Wrote " + str(i) + " products\r")
		logger.info("Wrote %s products to database", i)

if __name__ == "__main__":
	date = sys.argv[1] if len(sys.argv) == 2 else None
	with DbConnector(date) as connector:
		connector.insertAllProducts()