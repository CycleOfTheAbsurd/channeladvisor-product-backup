#!/usr/bin/env python
# -*- encoding: utf-8 -*-
import requests
import yaml
import pymongo
import datetime
import sys
import logging
from dateutil import parser

logger = logging.getLogger('myapp')
hdlr = logging.FileHandler(str(datetime.date.today()) + "-Channel.log")
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.INFO)

#Get config info
with open("config.yml", 'r') as ymlfile:
		cfg = yaml.load(ymlfile)

access_token = ""
client = pymongo.MongoClient("mongodb://%s:%s/" % (cfg["mongo"]["host"], cfg["mongo"]["port"]))
logger.info("Connected to local database %s on port %s", cfg["mongo"]["database"], cfg["mongo"]["port"])
db = client[cfg["mongo"]["database"]]
print("Loaded Config")
def getAuth():
	""" POSTS necessary authentication info to the API and updates the access token"""
	body = {"grant_type": "refresh_token", "refresh_token": cfg["refresh_token"]}
	resp = requests.post("https://api.channeladvisor.com/oauth2/token", body, auth=(cfg["app_id"], cfg["shared_secret"]))
	global access_token
	access_token = resp.json()["access_token"]
	logger.info("Received New Authentication Token")
	
def dateToString(dateF):
	"""Converts dates in multiple formats to ISO date string"""
	if type(dateF) != datetime.date:
		if type(dateF) == datetime.datetime:
			dateF = dateF.date()
		else:
			try:
				dateF = parser.parse(dateF)
			except (ValueError, AttributeError):
				dateF = date.today() - datetime.timedelta(days=7)
				print("Invalid date format. Using today - 1 week as cutoff date.")
	return dateF.isoformat()

def requestProductPage(url="", account_id=""):
	"""Builds the API url and return the response. 
	Filters by lastModified date if specified."""
	if url == "":
		body = {"access_token": access_token, "$expand": "Attributes"}
		#Add filter if modification date is specified.
		global lastModified
		if lastModified:
			body["$filter"] = "UpdateDateUtc ge " + lastModified + "Z and ProfileID eq " + account_id
		resp = requests.get("https://api.channeladvisor.com/v1/Products", body)
		logger.info("Initial api request")
		logger.info(resp.status_code)
	else:
		try:
			logger.info("Sent Request to %s", url)
			resp = requests.get(url)
			logger.info(resp.status_code)
		except requests.exceptions.ConnectionError as e:
				print(e)
				logger.error(e)
				logger.error("Connection Error", exc_info=True)
				sys.exit()
	return resp

def getProducts(account_id=""):
	"""Generator Function:
	   Goes through all the response's page yielding product one by one as dictionaries"""
	url = ""
	while True:
		resp = requestProductPage(url, account_id=account_id)
		#Reauthenticate if access is refused
		if resp.status_code == 403:
			getAuth()
			resp = requestProductPage(url)
		#If request is successful get response JSON and yield products individually
		if 200 <= resp.status_code < 300:
			rj = resp.json()
			for product in rj["value"]:
				yield product
			#Go to next page of response as long as there is one
			if rj["@odata.nextLink"] is None:
				break
			else:
				url = rj["@odata.nextLink"]
	return

def parseProductToDB(product, account_id=""):
	"""Performs any necessary data/attribute cleaning and transformation
	Then inserts the product into the database"""
	product["_id"] = product["ID"]
	product["Attributes"] = {attr["Name"]: attr["Value"] for attr in product["Attributes"]}
	#Put all printers in a list and delete individual attributes
	product["Attributes"]["Printers"] = [printer for key, printer in product["Attributes"].items() if key.startswith("CompatiblePrinter") and printer != "" and printer is not None]
	product["Images"] = {image["PlacementName"]: image["Url"] for image in product["Images"]}
	for k in list(product["Attributes"].keys()):
		if k.startswith("CompatiblePrinter"):
			del product["Attributes"][k]
	#Remove link to next page that sometimes appears in products for unknown reasons
	product.pop("Attributes@odata.nextLink", None)
	collection = cfg["mongo"]["collection"] + "_" + account_id
	#This is effectively an upsert
	try:
		db[collection].insert_one(product)
	except pymongo.errors.DuplicateKeyError:
		db[collection].update({"_id": product["_id"]}, product)

logger.info("Program Started")
lastModified = None
getAuth()
if len(sys.argv) == 2:
	lastModified = dateToString(sys.argv[1])
	print("Using %s as cutoff for last modified date" % lastModified)
for account_id in cfg["account_ids"]:
	for product in getProducts(account_id):
		parseProductToDB(product, account_id)