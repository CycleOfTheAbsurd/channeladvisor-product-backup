README
=========

This is a script to save all the hard work you put in creating your Channel Advisor products database.
Using the API, it saves all your product data to a local MongoDB database.

Requires:
* pymongo 
* pyyaml
* requests

---

Fill in the required fields in `config.sample.yml` and rename it `config.yml`
All of the required info can be found in your Developper Console

Created For [WeSellToners](http://www.weselltoners.com)
